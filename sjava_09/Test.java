//sjava_09: JSON

import com.google.gson.Gson;

public class Test {
    public static void main(String[] args) {
        Gson gson = new Gson();

        Persona p = new Persona("humbert", 22);

        //La siguiente sentencia es equivalente a las dos lineas siguientes.
        // gson.toJson(p, System.out);
        
        String str = gson.toJson(p);
        System.out.println(str);

        // {"nombre":"humbert","edad":22}
        String js = "{\"nombre\":\"antonio\",\"edad\":22}";
        Persona p2 = gson.fromJson(js, Persona.class);
        System.out.println(p2.toString());
    }
}