//sjava_09: JSON

class Persona {

    String nombre;
    int edad;

    Persona(String nombre, int edad) {
        this.nombre = nombre;
        this.edad = edad;
    }

    @Override
    public String toString() {
        return "nombre: " + this.nombre + " edad: " + this.edad ;
    }

}