//sjava_03: Argumentos en líneas de comandos

/*A la hora de compilar, tenemos que introducir la cadena de números en el terminal cuando escribamos el comando 
java SumaNums 12 44 56, por ejemplo*/

class SumaNumsA {
    public static void main(String[] args) {
        int total = 0;

        for (String s : args) {
            int num = Integer.parseInt(s);
            total += num;
        }

        System.out.println("El total es " + total + ".");
    }
}