//sjava_03: Argumentos en líneas de comandos

class InfoNums {

    public static void main(String[] args) {

        int cantidad = args.length;

        if (cantidad == 0){
            System.out.println("No se ha recibido ningún número!");
            return;
        }

        int total = 0;
        int counter = 0;
        int min = Integer.parseInt(args[0]);
        int max = Integer.parseInt(args[0]);
        float media;

        if (cantidad > 1) {

            for(int i = 0; i < args.length; i++) {

                int num = Integer.parseInt(args[i]);
                total += num;
                counter++;

            if (min > num){
                min = num;
            }

            if (max < num){
                max = num;
            }

            }

        media = (float) total/counter;

        System.out.println("La cantidad de números introducidos es " + counter + ".");
        System.out.println("La media aritmética de los números introducidos es " + media + ".");
        System.out.println("El máximo de los números introducidos es " + max + ".");
        System.out.println("El mínimo de los números introducidos es " + min + ".");

        //Otra forma de haber escrito los resultados por pantalla hubiese sido la siguiente

        System.out.printf("\nLa cantidad de números introducidos es %d.\n", counter);
        System.out.printf("La media aritmética de los números introducidos es %.2f.\n", media);
        System.out.printf("El máximo de los números introducidos es %d.\n", max);
        System.out.printf("El mínimo de los números introducidos es %d.\n", min);

        }

    }

}