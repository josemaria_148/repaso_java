//sjava_06: Arrays

import java.util.Scanner;

class Palindromo {

    public static void main(String[] args) {

        Scanner keyboard = new Scanner(System.in);
        StringBuilder sb = new StringBuilder();
        
        System.out.println("Introduce una palabra palindroma o no palindroma:");

        String texto = keyboard.next();

        texto = texto.toLowerCase();
        
        char[] chars = texto.toCharArray();

        chars[chars.length-1] = Character.toUpperCase(chars[chars.length-1]);

        for (int i = chars.length - 1; i >= 0; i--) {
            sb = sb.append(chars[i]);
        }

        System.out.println("El palindromo asociado a la palabra introducida es:");
        System.out.println(sb.toString());

    }

}