//sjava_08: Archivos

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.TreeSet;

public class Provincias {

	public static void main(String[] args) {
		//Declaramos la variable fin que contendrá las provincias del archivo provincias.txt
		File fin = new File("provincias.txt");
		/*Definimos la colección de provincias mediante la interfaz Set y TreeSet
		El TreeSet agrega elementos no repetidos y ordenados*/
		Set<String> provincias = new TreeSet<String>();

		// Esto es por si queremos mostrar todo el listado contenido en provincias.txt
		// List<String> provincias = new ArrayList<String>();

			try(
				/*Declaramos la variable fr (filereader) de tipo InputStreamReader
				Creamos un objeto de la clase InputStreamReader
				Entre paréntesis le pasamos el InputStream que queremos que lea*/
				InputStreamReader fr = new InputStreamReader(new FileInputStream (fin),"UTF8");

				/*La clase InputStreamReader nos devuelve caracteres sueltos
				Para decirle al InputStreamReader cuántos caracteres queremos, para lo cual utilizamos
				la clase BufferedReader, que nos devuelve un String (cadena de caracteres)*/
				BufferedReader br = new BufferedReader(fr);
			) {

				String linea;

				do {
					//Para pedirle el String, utilizamos el método readLine()
					linea = br.readLine();

					if(linea != null){
						provincias.add(linea);
					}
				
				} while (linea != null);

				for(String provincia : provincias) {
					System.out.println(provincia);
				}

			} catch (Exception e) {
				e.printStackTrace();
			}

	}

}