import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;
import java.io.File;


//sjava_08: Archivos

public class Hondas{

	public static void main(String[] args) {
		
		File fin = new File("motos.csv");
		File fout = new File("hondas.csv");
		List<String> hondas = new ArrayList<String>();

		String encontrar = "honda";

		try (
			FileReader fr = new FileReader(fin);
			BufferedReader br = new BufferedReader(fr);

			FileWriter fw = new FileWriter(fout);
			BufferedWriter bw = new BufferedWriter(fw);

		) {

			String line;

			do {
				line = br.readLine();

				if (line != null && line.toLowerCase().contains(encontrar)) {
					hondas.add(line);
				}
				
			} while (line != null);

			for (String s : hondas){
				bw.write(s);
				bw.newLine();
			}

			bw.flush();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
