

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;


public class Filtra {

	public static void main(String[] args) {

		/*En este caso, para filtar por marca de motos desde el archivo motos.csv, tras compilar el archivo Filtra.java,
		escribimos, por ejemplo, java Filtra Ducati*/

        String encontrar = args[0];

		File fin= new File("motos.csv");
		File fout = new File(encontrar+".csv");
	
		try (	FileWriter fw = new FileWriter(fout);
				BufferedWriter bw = new BufferedWriter(fw);

				FileReader fr = new FileReader(fin);
				BufferedReader br = new BufferedReader(fr);
				) {
			
			List<String> hondas = new ArrayList<String>();
			
			String line;
			do {
				line = br.readLine();
				if (line!=null && line.toLowerCase().contains(encontrar)) {
						hondas.add(line);
					}
			} while (line!=null);

			
			for (String s : hondas) {
				bw.write(s);
				bw.newLine();
			}
			
			bw.flush();

		} catch (Exception e) {
			e.printStackTrace();
		} 

	}

}
