//sjava_02:Clase TestPerson y Person

class PersonModificacion {
    String nombre;
    int edad;
    int diferenciaEdades;
 
    public PersonModificacion(String nombre, int edad){
        this.nombre = nombre;
        this.edad = edad;
    }

    public void comparaEdad(PersonModificacion otra){
        if (this.edad > otra.edad){
            System.out.printf("%s es mayor que %s.\n", this.nombre, otra.nombre);
        } else {
            System.out.printf("%s es mayor que %s.\n", otra.nombre, this.nombre);
        }
    }

/*Para que visualicemos tildes y caracteres especiales a la hora de compilar,
debemos escribir en la terminal javac -encoding "UTF8" *.java*/

    public void diferenciaEdad(PersonModificacion otra){
        if (this.edad > otra.edad){
            diferenciaEdades = this.edad - otra.edad;
            System.out.printf("%s es %s años mayor que %s.", this.nombre, diferenciaEdades, otra.nombre);
        } else if (this.edad < otra.edad) {
            diferenciaEdades = otra.edad - this.edad;
            System.out.printf("%s es %s años mayor que %s.", otra.nombre, diferenciaEdades, this.nombre);
        } else {
            System.out.printf("%s y %s tienen la misma edad.", this.nombre, otra.nombre);
        }
    }

}