//sjava_04: Entrada desde teclado

import java.util.Scanner;

class PideNumsB {

    public static void main(String[] args) {
        Scanner keyboard = new Scanner (System.in);
        int total = 0;
        int num = 1;

        do {
            System.out.println("Introduce un número: ");

            try {
                num = keyboard.nextInt();
                total += num;
            } catch (Exception e) {
                System.out.println("***Dato incorrecto --- 0 para salir***");
                //Importante escribir el comando keyboard.next(), porque si no, el bucle se repite indefinidamente
                keyboard.next();
            }

        } while (num > 0);

        System.out.println("La suma es " + total + ".");
        keyboard.close();
    }

}