//sjava_04: Entrada desde teclado

import java.util.Scanner;
import java.util.Random;

class Adivina {

    public static void main(String[] args) {
        
        Random random = new Random();
        Scanner keyboard = new Scanner(System.in);
        int counter = 0;
        int num = 1;
        int incognita = random.nextInt(10)+1;

        System.out.println("Advina un número comprendido entre 1 y 10");

        do{

            System.out.println("Introduce un número: ");
            counter++;
            
            try {
                num = keyboard.nextInt();
            } catch (Exception e) {
                System.out.println("***Error. Introduce otro número.***");
                keyboard.next();
            }

        } while (num != incognita);

        System.out.printf("Correcto. Tu número de intentos ha sido %d.", counter);
        keyboard.close();
        
    }

}