//sjava_04: Entrada desde teclado

import java.util.Scanner;

class InfoNumsK {

    public static void main(String[] args) {
        
        int total = 0;
        int num = 1;
        int counter = -1;
        Scanner keyboard = new Scanner(System.in);

        int min = 0;
        int max = 0;

        boolean primero = true;

        do {
            System.out.println("Introduce un número: ");

            try {
                num = keyboard.nextInt();
                total += num;
                counter++;

                if (primero){
                    min = num;
                    max = num;
                    primero = false;
                }

                if (min > num && num != 0) {
                    min = num;
                }

                if (max < num) {
                    max = num;
                }
            } catch (Exception e) {
                System.out.println("***Error --- Pulsa 0 para salir***");
                keyboard.next();
            }

        } while (num > 0);

        float media = (float) total/counter;

        System.out.printf("La cantidad de números introducidos es %d.\n", counter);
        System.out.printf("La suma de los números introducidos es %d.\n", total);
        System.out.printf("La media artimética de los números introducidos es %.2f.\n", media);
        System.out.printf("El mínimo de los números introducidos es %d.\n", min);
        System.out.printf("El máximo de los números introducidos es %d.\n", max);

        keyboard.close();

    }

}