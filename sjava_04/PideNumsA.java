//sjava_04: Entrada desde teclado

import java.util.Scanner;

class PideNumsA {

        public static void main(String[] args) {
            Scanner keyboard = new Scanner (System.in);
            int total = 0;
            int num = 0;

            do{
                System.out.println("Introduce un número: ");
                num = keyboard.nextInt();
                total += num;
            } while (num > 0);

            System.out.println("La suma es " + total + ".");
            keyboard.close();
        }

}